#include <ostream>

#include <cm/optional>

#include "cmGeneratedFileStream.h"
#include "cmGlobalCommonGenerator.h"
#include "cmGlobalGeneratorFactory.h"

#ifndef CMAKE_CMLINKDATABASEGENERATOR_H
#define CMAKE_CMLINKDATABASEGENERATOR_H

class cmLinkDatabaseGenerator
{
public:
  cmLinkDatabaseGenerator();
  void GenerateLinkCommand(const std::vector<std::string>& dependencyFiles,
                           const std::string& workingDirectory,
                           const std::vector<std::string>& linkCommands,
                           const std::string& version, bool isOnOption,
                           std::set<int> ranlibIndexes,
                           const std::string& buildFileDir);

  void CloseLinkCommandsStream();

private:
  const std::string newestSupportedFormat = "0.0.1";
  std::unique_ptr<cmGeneratedFileStream> LinkCommandStream;
  Json::Value LinkCommandsJson;
};

#endif // CMAKE_CMLINKDATABASEGENERATOR_H
